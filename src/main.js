import { Board } from './components/board'
import { Ball } from './components/ball'
import { Player } from './components/player'

import { LEFT, RIGHT } from './lib/glob'
import { K_W, K_S, K_SPACE } from './lib/keys'

const board = new Board()
const ball = new Ball(300, 150, 5, { x: 4, y: 4 }, board.context)
const enemy = new Player(595, 150, 10, 50, { x: 2, y: 2 }, board.context)
const player = new Player(5, 150, 10, 50, { x: 2, y: 2 }, board.context)

let keyPressed = false

function draw() {
  board.clear()
  board.draw()
  ball.draw()
  player.draw()
  enemy.draw()
}

function update() {
  const collision = ball.checkBorderCollision()
  if (collision) {
    // Initial Position of Components
    ball.reset()
    player.reset()
    enemy.reset()

    // Draw Components
    draw()

    // Update Scores
  } else {
    // Check Collisions between Components
    ball.checkPlayerCollision(player, LEFT)
    ball.checkPlayerCollision(enemy, RIGHT)

    // Move Components
    ball.move()
    player.move()
    enemy.move()

    // Draw Components
    draw()

    // New Update
    requestAnimationFrame(update)
  }
}

window.addEventListener('load', function() {
  board.draw()
  ball.draw()
  player.draw()
  enemy.draw()
})

window.addEventListener('keydown', function (ev) {
  if (ev.keyCode === K_W && !keyPressed) {
    keyPressed = true
    player.moveUp()
    enemy.moveUp()
  } else if (ev.keyCode === K_S && !keyPressed) {
    keyPressed = true
    player.moveDown()
    enemy.moveDown()
  }
})

window.addEventListener('keyup', function (ev) {
  if (ev.keyCode === K_W && keyPressed) {
    keyPressed = false
    player.stop()
    enemy.stop()
  } else if (ev.keyCode === K_S && keyPressed) {
    keyPressed = false
    player.stop()
    enemy.stop()
  } else if (ev.keyCode === K_SPACE) {
    update()
  }
})

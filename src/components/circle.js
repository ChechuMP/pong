import { Component } from './component'

export class Circle extends Component {
  constructor(x, y, r, context) {
    const width = 2 * r
    const height = 2 * r
    super(x, y, width, height, context)
    super.setRadius(r)
  }
  draw() {
    this.context.beginPath()
    this.context.arc(this.centerX, this.centerY, this.r, 0, 2 * Math.PI)
    this.context.fill()
  }
}

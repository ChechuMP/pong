import { Component } from './component'

export class Rect extends Component {
  draw() {
    this.context.fillRect(this.left, this.top, this.width, this.height)
  }
}

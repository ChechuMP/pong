import { Rect } from './rect'
import { UP, STOP, DOWN, LEFT, RIGHT, HEIGHT } from './../lib/glob'

export class Player extends Rect {
  constructor(x, y, width, height, speed, context) {
    super(x, y, width, height, context)
    this.speed = speed
    this.movement = STOP
  }
  move() {
    if (this.movement === UP && this.top > 0) {
      this.centerY -= this.speed.y
    } else if (this.movement === DOWN && this.bottom < HEIGHT) {
      this.centerY += this.speed.y
    } else {
      this.stop()
    }
    this.calculateAttributes()
  }
  moveUp() {
    this.movement = UP
  }
  moveDown() {
    this.movement = DOWN
  }
  stop() {
    this.movement = STOP
  }
  reset() {
    this.centerX = this.x0
    this.centerY = this.y0
    this.calculateAttributes()
  }
}

export class Board {
  constructor() {
    this.canvas = document.getElementById('game-canvas')
    this.context = this.canvas.getContext('2d')
  }
  draw() {
    this.context.beginPath()
    this.context.moveTo(this.canvas.width / 2, 0)
    this.context.lineTo(this.canvas.width / 2, this.canvas.height)
    this.context.stroke()
  }
  clear() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
  }
}

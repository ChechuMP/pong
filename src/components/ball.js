import { Circle } from './circle'
import { LEFT, RIGHT, WIDTH, HEIGHT } from './../lib/glob'

export class Ball extends Circle {
  constructor(x, y, r, speed, context) {
    super(x, y, r, context)
    this.speed = speed
  }
  move() {
    this.centerX += this.speed.x
    this.centerY += this.speed.y
    this.calculateAttributes()
  }
  reset() {
    this.centerX = this.x0
    this.centerY = this.y0
    this.speed.x = (Math.random() < 0.5) ? -1 * this.speed.x : this.speed.x
    this.speed.y = (Math.random() < 0.5) ? -1 * this.speed.y : this.speed.y
    this.calculateAttributes()
  }
  checkBorderCollision() {
    if (this.left <= 0 || this.right >= WIDTH) {
      return 1
    } else if (this.top <= 0 || this.bottom >= HEIGHT) {
      this.speed.y = -1 * this.speed.y
    }
    return 0
  }
  checkPlayerCollision(player, side) {
    if (this.top >= player.top && this.bottom <= player.bottom) {
      if (this.left <= player.right && side === LEFT) {
        this.speed.x = -1 * this.speed.x
      } else if (this.right >= player.left && side === RIGHT) {
        this.speed.x = -1 * this.speed.x
      }
    }
  }
}

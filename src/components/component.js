export class Component {
  constructor(x, y, width, height, context) {
    // Basic properties of my component
    this.x0 = x
    this.y0 = y
    this.centerX = x
    this.centerY = y
    this.width = width
    this.height = height
    this.context = context

    // Radius property of my component (NULL by default)
    this.r = null

    // Calculate initial values of positional properties
    this.calculateAttributes()
  }
  calculateAttributes() {
    // Face position attributes of my component
    this.top = this.centerY - this.height / 2
    this.left = this.centerX - this.width / 2
    this.right = this.centerX + this.width / 2
    this.bottom = this.centerY + this.height / 2
  }
  setRadius(radius) {
    this.r = radius
  }
}

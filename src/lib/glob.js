export const UP = 'UP'
export const DOWN = 'DOWN'
export const STOP = 'STOP'
export const LEFT = 'LEFT'
export const RIGHT = 'RIGHT'
export const WIDTH = 600
export const HEIGHT = 300

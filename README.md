### About
This is a simple pong game develop with pure JS for learning purpose.  
___

### Tools
I use several tools to make easier the development:
- [Babel](https://babeljs.io/): latest features of JS running in any browser.
- [ESLint](https://eslint.org/): linting utility for JS.
- [Browserify](http://browserify.org/): this library allows you require modules in the browser.
___

### Development
You can clone and install the dependencies following these steps:
```bash
$ git clone git@gitlab.com:ChechuMP/pong.git
$ cd pong
$ npm install
```

The file `package.json` provides two commands to build the game with your changes:  
```bash
$ npm run clean # Clean the /dist/bundle.js file
$ npm run build # Compile thegame using babel and browserify
```

### Play
At the moment the only way to play the game is cloning the repository, building it and opening in the browser the file `/dist/index.html`.
